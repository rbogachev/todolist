#!/bin/bash

if /wait-for-it.sh -t $POSTGRES_WAIT_TIME $POSTGRES_HOST:$POSTGRES_PORT; then
  exec "$@"
else
  echo -e "[ERROR]: Postgres wait time expired.\n"
  exit -1
fi
