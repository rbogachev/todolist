from fastapi import FastAPI
from database.database import init_db
from core.config import TITLE, VERSION, DOCS_URL, REDOC_URL, OPENAPI_URL, API_PATH_V1
from api.v1.endpoints import api_router as api_v1_router, API_V1_META_TAGS


TAGS_METADATA = API_V1_META_TAGS

init_db()
app = FastAPI(
    title=TITLE,
    version=VERSION,
    docs_url=DOCS_URL,
    redoc_url=REDOC_URL,
    openapi_url=OPENAPI_URL,
    openapi_tags=TAGS_METADATA,
)

app.include_router(api_v1_router, prefix=API_PATH_V1)
