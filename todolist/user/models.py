from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from database.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    password = Column(String)
    role = Column(String)  # TODO make as many-to-many relationship with Roles table

    boards = relationship("Board", back_populates="creator")
