from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm.session import Session

from core.config import API_PATH_V1
from core.dependencies import get_db_session
from core.dependencies import get_user_from_token
from cryptography.cryptography import verify_password
from jwt.jwt import create_access_token
from jwt.schemas import Token

from user.models import User
from user.schemas import UserCreate, UserGet
from user.queries import user_crud


TAGS_METADATA = {
    "name": "users",
    "description": "Operations with users: sign-up, login and get access token",
}

router = APIRouter(prefix="/users", tags=[TAGS_METADATA["name"]])


@router.post("/signup", response_model=UserGet)
def create_user(user: UserCreate, db: Session = Depends(get_db_session)):
    db_user = user_crud.get_by_username(db, user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="User already exists")
    return user_crud.create(db, user, role="everyone")


@router.post("/login", response_model=Token)
def login_user_and_create_access_token(
    db: Session = Depends(get_db_session),
    form_data: OAuth2PasswordRequestForm = Depends(),
):
    user = user_crud.get_by_username(db, form_data.username)
    if not user or not verify_password(form_data.password, user.password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token(data={"sub": user.username})
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/me", response_model=UserGet)
def get_user_me(current_user: User = Depends(get_user_from_token)):
    return current_user
