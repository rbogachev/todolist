from typing import Optional
from sqlalchemy.orm import Session
from cryptography.cryptography import hash_password
from .models import User
from core.crud import CRUDBase
from .schemas import UserCreate, UserUpdate


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    def create(self, db: Session, create_obj_schema: UserCreate, **kwargs) -> User:
        create_obj_schema.password = hash_password(create_obj_schema.password)
        return super().create(db, create_obj_schema, **kwargs)

    def get_by_username(self, db: Session, username: str) -> Optional[User]:
        return db.query(self.model).filter(self.model.username == username).first()


user_crud = CRUDUser(User)
