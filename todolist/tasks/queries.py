from typing import List, Optional
from sqlalchemy.orm import Session
from .models import Task
from .schemas import TaskCreate, TaskUpdate
from user.models import User
from board.models import Board
from core.crud import CRUDBase


class CRUDTask(CRUDBase[Task, TaskCreate, TaskUpdate]):
    def get_by_owner(self, db: Session, user: User, id: int) -> Optional[Task]:
        task = super().get(db, id)
        if not task or task.board.creator != user:
            return None
        return task

    def get_multi_by_board(self, db: Session, board: Board) -> List[Task]:
        return db.query(self.model).filter(self.model.board == board).all()

    def get_multi_by_owner(self, db: Session, user: User) -> List[Task]:
        # TODO: for some reason commented out query doesn't work
        # return db.query(self.model).filter(self.model.board.creator == user).all()
        return db.query(self.model).join(Board).filter(Board.creator == user).all()


task_crud = CRUDTask(Task)
