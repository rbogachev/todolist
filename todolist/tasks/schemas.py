from pydantic import BaseModel, Field, validator
from datetime import datetime
from typing import Optional, Dict, Any


class BaseTask(BaseModel):
    description: str = Field(..., max_length=255)
    deadline: datetime
    is_completed: bool = False
    completed_on: Optional[datetime] = None


class TaskCreate(BaseTask):
    @validator("completed_on")
    def completetion_check(cls, v, values, **kwargs):
        if values["is_completed"] == True and v is None:
            raise ValueError("completed_on was not provided")
        if values["is_completed"] == False and v:
            raise ValueError(
                "completed_on was provided, but is_completed was set to False"
            )
        return v


class TaskUpdate(TaskCreate):
    description: Optional[str] = Field(None, max_length=255)
    deadline: Optional[datetime] = None
    is_completed: Optional[bool] = None
    completed_on: Optional[datetime] = None


class TaskGet(BaseTask):
    id: int
    board_id: int
    created_at: datetime

    class Config:
        orm_mode = True


class TaskDelete(BaseModel):
    status: str
