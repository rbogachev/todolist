from typing import List, Optional
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm.session import Session

from user.models import User
from core.dependencies import get_db_session
from core.dependencies import get_user_from_token
from core.config import API_PATH_V1
from board.endpoints.v1.endpoints import find_board_or_raise_exception

from tasks.schemas import TaskCreate, TaskUpdate, TaskGet, TaskDelete
from tasks.queries import task_crud
from tasks.models import Task


TAGS_METADATA = {
    "name": "tasks",
    "description": "CRUD operations with user's tasks",
}

router = APIRouter(prefix="/tasks", tags=[TAGS_METADATA["name"]])


def find_task_or_raise_exception(task_id: int, db: Session, user: User) -> Task:
    task = task_crud.get_by_owner(db, user, task_id)
    if task is None:
        raise HTTPException(status_code=404, detail="Not found")
    return task


@router.post("/create/{board_id}", response_model=TaskGet)
def create_task(
    board_id: int,
    task: TaskCreate,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    board = find_board_or_raise_exception(board_id, db, user)
    db_task = task_crud.create(db, task, board_id=board.id)
    return db_task


@router.put("/update/{task_id}", response_model=TaskGet)
def update_task(
    task_id: int,
    task_data: TaskUpdate,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    task = find_task_or_raise_exception(task_id, db, user)
    updated_task = task_crud.update(db, task, task_data)
    return updated_task


@router.delete("/delete/{task_id}", response_model=TaskDelete)
def delete_task(
    task_id: int,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    task = find_task_or_raise_exception(task_id, db, user)
    task_crud.delete(db, task)
    return {"status": "Deleted"}


@router.get("/list", response_model=List[TaskGet])
def list_tasks(
    user: User = Depends(get_user_from_token), db: Session = Depends(get_db_session)
):
    return task_crud.get_multi_by_owner(db, user)


@router.get("/list/{board_id}", response_model=List[TaskGet])
def list_board_tasks(
    board_id: int,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    board = find_board_or_raise_exception(board_id, db, user)
    return task_crud.get_multi_by_board(db, board)


@router.get("/get/{task_id}", response_model=TaskGet)
def get_task(
    task_id: int,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    return find_task_or_raise_exception(task_id, db, user)
