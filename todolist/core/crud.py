from typing import Any, Optional, Type, TypeVar, Generic, List
from sqlalchemy.orm.session import Session
from database.database import Base
from pydantic import BaseModel


ModelType = TypeVar("ModelType", bound=Base)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class CRUDBase(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
    """
    CRUD (create, read, update, delete) operations with SQLAlchemy models.
    """

    def __init__(self, model: Type[ModelType]) -> None:
        self.model = model

    def create(
        self, db: Session, create_obj_schema: CreateSchemaType, **kwargs
    ) -> ModelType:
        db_obj = self.model(**create_obj_schema.dict(), **kwargs)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get(self, db: Session, id: Any) -> Optional[ModelType]:
        return db.query(self.model).filter(self.model.id == id).first()

    def get_multi(
        self, db: Session, skip: int = 0, limit: int = 100
    ) -> List[ModelType]:
        return db.query(self.model).offset(skip).limit(limit).all()

    def update(
        self, db: Session, obj: ModelType, update_obj_schema: UpdateSchemaType
    ) -> ModelType:
        for attribute, value in update_obj_schema.dict(exclude_unset=True).items():
            setattr(obj, attribute, value)
        db.commit()
        db.refresh(obj)
        return obj

    def delete(self, db: Session, obj: ModelType) -> None:
        db.delete(obj)
        db.commit()
