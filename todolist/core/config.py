TITLE = "Todolist"
VERSION = "0.0.1"
API_PATH_V1 = "/api/v1"

DOCS_URL = "/api/docs"
REDOC_URL = "/api/redoc"
OPENAPI_URL = "/api/openapi.json"
