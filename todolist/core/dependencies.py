from typing import Generator
from database.database import SessionLocal
from fastapi.security import OAuth2PasswordBearer
from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session
from jwt.jwt import decode_access_token
from user.models import User
from user.queries import user_crud
from core.config import API_PATH_V1


def get_db_session() -> Generator:
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


oauth2_scheme = OAuth2PasswordBearer(tokenUrl=f"{API_PATH_V1}/users/login")


def get_user_from_token(
    db: Session = Depends(get_db_session), token: str = Depends(oauth2_scheme)
) -> User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    username: str = decode_access_token(token, "sub")
    if not username:
        raise credentials_exception

    user = user_crud.get_by_username(db, username)

    if not user:
        raise credentials_exception

    return user
