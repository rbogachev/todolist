from fastapi import APIRouter

from user.endpoints.v1.endpoints import (
    router as user_router,
    TAGS_METADATA as USER_TAGS,
)
from board.endpoints.v1.endpoints import (
    router as board_router,
    TAGS_METADATA as BOARD_TAGS,
)
from tasks.endpoints.v1.endpoints import (
    router as tasks_router,
    TAGS_METADATA as TASKS_TAGS,
)


API_V1_META_TAGS = [USER_TAGS, BOARD_TAGS, TASKS_TAGS]

api_router = APIRouter()
api_router.include_router(user_router)
api_router.include_router(board_router)
api_router.include_router(tasks_router)
