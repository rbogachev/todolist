from typing import List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm.session import Session

from user.models import User
from core.dependencies import get_db_session
from core.dependencies import get_user_from_token

from board.schemas import BoardCreate, BoardUpdate, BoardGet, BoardDelete
from board.queries import board_crud
from board.models import Board


TAGS_METADATA = {
    "name": "boards",
    "description": "CRUD operations with user's boards",
}

router = APIRouter(prefix="/boards", tags=[TAGS_METADATA["name"]])


def find_board_or_raise_exception(board_id: int, db: Session, user: User) -> Board:
    board = board_crud.get_by_owner(db, user, board_id)
    if board is None:
        raise HTTPException(status_code=404, detail="Not found")
    return board


@router.post("/create", response_model=BoardGet)
def create_board(
    board: BoardCreate,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    db_board = board_crud.create(db, board, creator_id=user.id)
    return db_board


@router.put("/update/{board_id}", response_model=BoardGet)
def update_board(
    board_id: int,
    board_data: BoardUpdate,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    board = find_board_or_raise_exception(board_id, db, user)
    updated_board = board_crud.update(db, board, board_data)
    return updated_board


@router.delete("/delete/{board_id}", response_model=BoardDelete)
def delete_board(
    board_id: int,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    board = find_board_or_raise_exception(board_id, db, user)
    board_crud.delete(db, board)
    return {"status": "Deleted"}


@router.get("/list", response_model=List[BoardGet])
def list_boards(
    user: User = Depends(get_user_from_token), db: Session = Depends(get_db_session)
):
    return board_crud.get_multi_by_owner(db, user)


@router.get("/get/{board_id}", response_model=BoardGet)
def get_board(
    board_id: int,
    user: User = Depends(get_user_from_token),
    db: Session = Depends(get_db_session),
):
    return find_board_or_raise_exception(board_id, db, user)
