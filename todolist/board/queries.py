from typing import List, Optional
from sqlalchemy.orm import Session
from .models import Board
from user.models import User
from .schemas import BoardCreate, BoardUpdate
from core.crud import CRUDBase


class CRUDBoard(CRUDBase[Board, BoardCreate, BoardUpdate]):
    def get_by_owner(self, db: Session, user: User, id: int) -> Optional[Board]:
        board = super().get(db, id)
        if not board or board.creator != user:
            return None
        return board

    def get_multi_by_owner(self, db: Session, user: User) -> List[Board]:
        return db.query(self.model).filter(self.model.creator == user).all()


board_crud = CRUDBoard(Board)
