from pydantic import BaseModel, Field
from datetime import datetime
from typing import Optional


class BaseBoard(BaseModel):
    name: str = Field(..., max_length=100)
    name_color: str = Field(
        ..., max_length=20, regex="^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"
    )
    background_color: str = Field(
        ..., max_length=20, regex="^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"
    )


class BoardCreate(BaseBoard):
    pass


class BoardUpdate(BaseModel):
    name: Optional[str] = Field(None, max_length=100)
    name_color: Optional[str] = Field(
        None, max_length=20, regex="^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"
    )
    background_color: Optional[str] = Field(
        None, max_length=20, regex="^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"
    )


class BoardGet(BaseBoard):
    id: int
    created_at: datetime

    class Config:
        orm_mode = True


class BoardDelete(BaseModel):
    status: str
