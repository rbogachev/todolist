# Run with Docker
```shell
docker-compose build
docker-compose run
```

# Browsable API
- **localhost:8000/api/docs** - Swagger UI
- **localhost:8000/api/redoc** - Redoc

## Pgagmin credentials
- Access: **localhost:5050**
- Admin email: admin@admin.com
- Admin password: postgres